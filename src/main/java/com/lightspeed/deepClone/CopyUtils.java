package com.lightspeed.deepClone;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class CopyUtils {

    private static final Set<Class<?>> WRAPPER_TYPES = Set.of(
            Boolean.class,
            Character.class,
            Byte.class,
            Short.class,
            Integer.class,
            Long.class,
            Float.class,
            Double.class
    );

    public static <T> T deepCopy(T obj) throws InstantiationException, IllegalAccessException {
        if (Objects.isNull(obj)) {
            return null;
        }

        Class<?> clazz = obj.getClass();
        if (clazz.isPrimitive() || WRAPPER_TYPES.contains(clazz) || String.class.equals(clazz)) {
            return obj;
        }

        if (clazz.isArray()) {
            int length = Array.getLength(obj);
            @SuppressWarnings("unchecked")
            T newArray = (T) Array.newInstance(clazz.getComponentType(), length);
            for (int i = 0; i < length; i++) {
                Array.set(newArray, i, deepCopy(Array.get(obj, i)));
            }
            return newArray;
        }

        if (obj instanceof Collection<?> originalCollection) {
            Collection<Object> newCollection;
            if (obj instanceof List<?>) {
                newCollection = new ArrayList<>();
            } else if (obj instanceof Set<?>) {
                newCollection = new HashSet<>();
            } else {
                newCollection = new ArrayList<>();
            }
            for (Object item : originalCollection) {
                newCollection.add(deepCopy(item));
            }
            @SuppressWarnings("unchecked")
            T typedCollection = (T) newCollection;
            return typedCollection;
        }

        @SuppressWarnings("unchecked")
        T newInstance = (T) clazz.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            Object originalFieldValue = field.get(obj);
            Object copiedFieldValue = deepCopy(originalFieldValue);
            field.set(newInstance, copiedFieldValue);
        }
        return newInstance;
    }
}
