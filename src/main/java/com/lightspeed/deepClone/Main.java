package com.lightspeed.deepClone;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class Main {

    public static void main(String[] args) {

        List<String> books = List.of("Metro2033", "Black Holes and Baby Universes");
        Man halk = new Man("Halk", 23, new ArrayList<>(books));

        try {
            Man deepCopyHalk = CopyUtils.deepCopy(halk);

            assertNotEquals(halk, deepCopyHalk);

            assertEquals(halk.getName(), deepCopyHalk.getName());
            assertEquals(halk.getAge(), deepCopyHalk.getAge());
            assertEquals(halk.getFavoriteBooks(), deepCopyHalk.getFavoriteBooks());

            halk.getFavoriteBooks().add("Fathers and Sons");
            assertEquals(3, halk.getFavoriteBooks().size());
            assertEquals(2, deepCopyHalk.getFavoriteBooks().size());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("The deep cloning process was successful");
    }
}
